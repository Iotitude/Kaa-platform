import java.io.IOException;
import java.util.Random;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pi4j.io.gpio.*;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;

import org.kaaproject.kaa.client.DesktopKaaPlatformContext;
import org.kaaproject.kaa.client.Kaa;
import org.kaaproject.kaa.client.KaaClient;
import org.kaaproject.kaa.client.SimpleKaaClientStateListener;
import org.kaaproject.kaa.client.configuration.base.ConfigurationListener;
import org.kaaproject.kaa.client.configuration.base.SimpleConfigurationStorage;
import org.kaaproject.kaa.client.logging.strategies.RecordCountLogUploadStrategy;

import iotitude.kaaplat.endpoint.config.HarmitusConfig01;
import iotitude.kaaplat.endpoint.log.HarmitusLog;


public class HarmitusApp {

    private static final Logger LOG = LoggerFactory.getLogger(HarmitusApp.class);

    private GpioController gpio;
    private boolean running_on_raspi = true;
    
    private static ScheduledFuture<?> scheduledFuture;
    private static ScheduledExecutorService scheduledExecutorService;

    // Here we define the GPIO pins that our physical buttons are attached to
    // http://pi4j.com/images/j8header-3b-large.png
    private GpioPinDigitalOutput ledPin;
    private final Pin[] pins = { RaspiPin.GPIO_07, RaspiPin.GPIO_00, RaspiPin.GPIO_02, RaspiPin.GPIO_03 };
    private ArrayList<GpioPinDigitalInput> buttons = new ArrayList<GpioPinDigitalInput>();

    private static KaaClient kaaClient;

    public static void main(String args[]) throws InterruptedException {
        LOG.info("STARTING");
        HarmitusApp app = new HarmitusApp();
        app.run();
    }

    private void initializeButtons() {
        LOG.info("Initializing buttons..");
        for (int i = 0; i < pins.length; i++) {
            Pin pin = pins[i];
            GpioPinDigitalInput button = gpio.provisionDigitalInputPin(pin, PinPullResistance.PULL_UP);
            button.setShutdownOptions(true);
            
            buttons.add(button);

            int buttonNumber = buttons.indexOf(button);

            button.addListener(new GpioPinListenerDigital() {
                @Override
                public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {
                    // display pin state on console
                    LOG.info(" --> GPIO PIN STATE CHANGE: " + event.getPin() + " = " + event.getState());
                    if (event.getState() == PinState.LOW) {
                        LOG.info("Button " + buttonNumber + " was pressed!");
                        ledPin.pulse(1000);
                        kaaClient.addLogRecord(new HarmitusLog(buttonNumber, getUnixTimestamp()));
                    }
                }
            });
        }
    }

    private void run() {
        try {
            gpio = GpioFactory.getInstance();
            scheduledExecutorService = Executors.newScheduledThreadPool(1);
        } catch (UnsatisfiedLinkError e) {
            running_on_raspi = false;
        }

        DesktopKaaPlatformContext desktopKaaPlatformContext = new DesktopKaaPlatformContext();
        kaaClient = Kaa.newClient(desktopKaaPlatformContext, new FirstKaaClientStateListener(), true);

        RecordCountLogUploadStrategy strategy = new RecordCountLogUploadStrategy(1);
        strategy.setMaxParallelUploads(1);
        kaaClient.setLogUploadStrategy(strategy);

        kaaClient.setConfigurationStorage(new SimpleConfigurationStorage(desktopKaaPlatformContext, "saved_config.cfg"));

        kaaClient.addConfigurationListener(new ConfigurationListener() {
            @Override
            public void onConfigurationUpdate(HarmitusConfig01 configuration) {
                LOG.info("Received configuration data. New sample period: {}", configuration.getInterval());
            }
        });

        if (running_on_raspi) {
            ledPin = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_04, "MyLED", PinState.LOW);
            ledPin.setShutdownOptions(true, PinState.LOW);
            initializeButtons();
        }

        kaaClient.start();
        LOG.info("--= Press any key to exit =--");
        
        /*
        try {
            System.in.read();
        } catch (IOException e) {
            LOG.error("IOException has occurred: {}", e.getMessage());
        }
        */
        
        int i = 1;
        
        while (i == 1) {
            try {
                Thread.sleep(100);
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
        }

        LOG.info("Stopping...");
        if (running_on_raspi) {
            gpio.shutdown();
            scheduledExecutorService.shutdown();
        }
        kaaClient.stop();
    }

    private long getUnixTimestamp() {
        return System.currentTimeMillis() / 1000L;
    }
    
    
    private int getHarmitusValue() {
      return new Random().nextInt(4);
    }

    private void onKaaStarted(long time) {
        if (time <= 0) {
            LOG.error("Wrong time is used. Please, check your configuration!");
            kaaClient.stop();
            System.exit(0);
        }
        if (running_on_raspi) {
            LOG.error("THIS IS A RASPI");
            return;
        }
        scheduledFuture = scheduledExecutorService.scheduleAtFixedRate(
                new Runnable() {
                    @Override
                    public void run() {
                        int value = getHarmitusValue();
                        kaaClient.addLogRecord(new HarmitusLog(value, getUnixTimestamp()));
                        LOG.info("Harmitus level: {}", value);
                    }
                }, 0, time, TimeUnit.MILLISECONDS);
    }

    private class FirstKaaClientStateListener extends SimpleKaaClientStateListener {
        @Override
        public void onStarted() {
            super.onStarted();
            LOG.info("Kaa client started");
            HarmitusConfig01 configuration = kaaClient.getConfiguration();
            LOG.info("Default interval: {}", configuration.getInterval());
            onKaaStarted(TimeUnit.SECONDS.toMillis(configuration.getInterval()));
        }

        @Override
        public void onStopped() {
            super.onStopped();
            LOG.info("Kaa client stopped");
        }
    }
}
